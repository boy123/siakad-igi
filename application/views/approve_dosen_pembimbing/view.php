<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered" id="datatable">
			<thead>
				<tr>
					<th>No.</th>
					<th>Nim</th>
					<th>Nama</th>
					<th>Semester</th>
					<th>Jurusan</th>
					<th>Option</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$no = 1;
				$id_users = $this->session->userdata('keterangan');

				$cek_ka_prodi = $this->db->get_where('akademik_konsentrasi', array('ka_prodi'=>$id_users));
				if ($cek_ka_prodi->num_rows() == 1) {
					$konsentrasi_id = $cek_ka_prodi->row()->konsentrasi_id;
					$sql = "SELECT * FROM student_mahasiswa where nim in (SELECT nim FROM akademik_krs ) and konsentrasi_id='$konsentrasi_id' and status_mhs='Aktif' ";
				} else {
					$sql = "SELECT * FROM student_mahasiswa where nim in (SELECT nim FROM akademik_krs ) and dosen_pa = '$id_users' and status_mhs='Aktif' ";
				}

				
				foreach ($this->db->query($sql)->result() as $rw) {
				 ?>
				<tr>
					<td><?php echo $no; ?></td>
					<td><?php echo $rw->nim; ?></td>
					<td><?php echo $rw->nama; ?></td>
					<td><?php echo $rw->semester_aktif; ?></td>
					<td><?php echo get_data('akademik_konsentrasi','konsentrasi_id',$rw->konsentrasi_id,'nama_konsentrasi'); ?></td>
					<td>
						<?php 
						$cek_ = $this->db->get_where('akademik_krs', array('nim'=>$rw->nim,'semester'=>$rw->semester_aktif));
						if ($cek_->row()->approve != '1') {
						?>
							<!-- <a href="<?php echo base_url() ?>manual/detail_krs/<?php echo $rw->nim ?>" class="btn btn-danger">Approve</a> -->
							<a href="<?php echo base_url() ?>manual/approve/<?php echo $rw->nim.'/'.$rw->semester_aktif ?>" class="btn btn-danger">Approve</a>
						<?php } else { ?>
							<a class="btn btn-success">Disetujui</a>
						<?php } ?>
						<a href="<?php echo base_url() ?>manual/detail_krs/<?php echo $rw->nim.'/'.$rw->semester_aktif ?>" class="btn btn-success"> Lihat</a>
					</td>
				</tr>
				<?php $no++; } ?>
			</tbody>
		</table>
	</div>
</div>