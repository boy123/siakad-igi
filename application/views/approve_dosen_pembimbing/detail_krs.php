<?php error_reporting(1) ?>


<div class="row">
  <div class="col-sm-12">
        <table class='table table-bordered'>
             <?php
                    $id   = get_data('student_mahasiswa','nim',$this->uri->segment(3),'mahasiswa_id');

                    $mhs  =   "SELECT sm.nim,sm.nama,sm.semester_aktif,ap.nama_prodi,ak.nama_konsentrasi,sm.kelas_kuliah
                                FROM student_mahasiswa as sm,akademik_konsentrasi as ak,akademik_prodi as ap
                                WHERE ap.prodi_id=ak.prodi_id and sm.konsentrasi_id=ak.konsentrasi_id and sm.mahasiswa_id=$id";
                    $thn           =  get_tahun_ajaran_aktif('tahun_akademik_id');
                    $d    = $this->db->query($mhs)->row();
                    $nim  =  getField('student_mahasiswa', 'nim', 'mahasiswa_id', $id);
                    $krs  =   "SELECT ak.krs_id,mm.kode_makul,mm.nama_makul,mm.sks,ad.nama_lengkap
                                FROM makul_matakuliah as mm,akademik_jadwal_kuliah as jk,akademik_krs as ak,app_dosen as ad
                                WHERE mm.makul_id=jk.makul_id and ad.dosen_id=jk.dosen_id and jk.jadwal_id=ak.jadwal_id and ak.tahun_akademik_id='$thn' and ak.nim='$nim' and ak.semester='".$d->semester_aktif."'";
                    $data =  $this->db->query($krs);

                ?>
            <?php
                if ($d->semester_aktif !=0) {
                    ?>
            <tr>
                <td colspan=6>


                </td>
            </tr>
                    <?php
                }
            ?>
            <tr>
                <td width='150'>NAMA</td><td><?php echo strtoupper($d->nama); ?></td>
                <td width=100>NIM</td><td><?php echo strtoupper($d->nim)?></td><td rowspan='2' width='70'><img width='50' src=<?php echo base_url()."assets/images/avatar.png"?> ></td>
            </tr>
            <tr>
                <td>Prodi / Kelas </td><td><?php echo strtoupper($d->nama_konsentrasi.' / '.$d->kelas_kuliah); ?></td>
                <td>SEMESTER</td><td><?php echo $d->semester_aktif; ?> </td>
            </tr>
        </table>
        <?php
        $max_sks = 30;
        ?>
        <table id="my-grid" class='table table-bordered'>
            <tr class='alert-info'><th width='5'>No</th>
                <th width='80'>KODE</th>
                <th>NAMA MATAKULIAH</th>
                <th width=10>SKS</th>
                <th>DOSEN PENGAPU</th>
            </tr>
            <?php

                $sks=0;
                if($data->num_rows()<1)
                {
                    echo "<tr><td colspan=6 align='center' class='warning'>DATA KRS TIDAK DITEMUKAN</td></tr>";
                }
                else
                {
                    $no=1;

                    foreach ($data->result() as $r)
                    {
                        echo "<tr id='krshide$r->krs_id'>
                            <td align='center'>$no</td>
                            <td align='center'>".  strtoupper($r->kode_makul)."</td>
                            <td>".  strtoupper($r->nama_makul)."</td>
                            <td align='center'>".  $r->sks."</td>
                            <td>".  strtoupper($r->nama_lengkap)."</td>";
                        $no++;
                        $sks=$sks+$r->sks;
                    }
                }
            if ($sks>$max_sks) { ?>
                <script>
                    alert("INFO: SKS Lebih Dari <?php echo "$max_sks"; ?> SKS !! Silahkan Di kurangi");
                </script>
            <?php
            }?>
            <tr><td colspan='3' align='right'>Total SKS</td><td align='center'><?php echo $sks //if($sks>$max_sks){echo $sks;}else{echo "<b id='jumlah_sks'></b>";} ?><b id='jumlah_sks'></b></td><td colspan=2></td></tr>

        </table>

  </div>
</div>
