<div class="row">
	<div class="col-md-12">
		<form action="<?php echo base_url() ?>kalender_akademik/update" enctype="multipart/form-data" method="POST">
			<div class="form-group">
				<label>Upload Kalender Akademik</label>
				<input type="file" name="userfile" class="form-control">
				<p>
					*) File sebelumnya <br>
					<a href="<?php echo base_url() ?>images/kalender/<?php echo $img_kalender ?>" target="_blank">
						<img src="<?php echo base_url() ?>images/kalender/<?php echo $img_kalender ?>" style="width: 200px;">
					</a>
				</p>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary">UPDATE</button>
			</div>
		</form>
	</div>
</div>