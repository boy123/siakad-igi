<div class="row">
	
	<div class="col-md-6">
		<h2>Export PMB dari P2K</h2>
		<form action="<?php echo base_url() ?>p2k/export_p2k_pmb" target="_blank" method="GET">
			<div class="form-group">
				<label>Tahun</label>
				<select class="form-control" name="tahun" required="">
					<option value="<?php echo date('Y') ?>"><?php echo date('Y') ?></option>
					<option value="<?php echo (date('Y')-1) ?>"><?php echo (date('Y')-1) ?></option>
					<option value="<?php echo (date('Y')-2) ?>"><?php echo (date('Y')-2) ?></option>
				</select>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Export</button>
			</div>
		</form>
		
	</div>

	<div class="col-md-6">
		<br>
		
	</div>
</div>

<div class="row">
	
	<div class="col-md-6">
		<h2>Export Pembayaran dari P2K</h2>
		<form action="<?php echo base_url() ?>p2k/export_p2k_pembayaran" target="_blank" method="GET">
			<div class="form-group">
				<label>Bulan</label>
				<select class="form-control" name="bulan" >
					<option value="">--ALL--</option>
					<option value="1">01</option>
					<option value="2">02</option>
					<option value="3">03</option>
					<option value="4">04</option>
					<option value="5">05</option>
					<option value="6">06</option>
					<option value="7">07</option>
					<option value="8">08</option>
					<option value="9">09</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
				</select>
			</div>
			<div class="form-group">
				<label>Tahun</label>
				<select class="form-control" name="tahun" required="">
					<option value="<?php echo date('Y') ?>"><?php echo date('Y') ?></option>
					<option value="<?php echo (date('Y')-1) ?>"><?php echo (date('Y')-1) ?></option>
					<option value="<?php echo (date('Y')-2) ?>"><?php echo (date('Y')-2) ?></option>
				</select>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Export</button>
			</div>
		</form>
		
	</div>

	<div class="col-md-6">
		<br>
		
	</div>
</div>