<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php 
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=data_pmb_p2k.xls");
	$tahun = $this->input->get('tahun');
	 ?>

	<center>
		<h2>PENERIMAAN MAHASISWA BARU DARI P2K</h2>
		<h2>TAHUN <?php echo $tahun ?></h2>
	</center>
	<br><br>

	<table border="1">
		<thead>
			<tr>
				<th>NO.</th>
				<th>NISN</th>
				<th>NIK</th>
				<th>NAMA</th>
				<th>TEMPAT LAHIR</th>
				<th>TANGGAL LAHIR</th>
				<th>RT</th>
				<th>RW</th>
				<th>ALAMAT</th>
				<th>NO TELP</th>
				<th>TAHUN DAFTAR</th>
				<th>KELAS</th>
				<th>BIAYA PENDAFTARAN</th>
				<th>BIA BAYAR</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			$no = 1;
			$this->db->where('tahun_daftar', $tahun);
			$data = $this->db->get('p2k_pmb');
			foreach ($data->result() as $rw) {
			 ?>
			<tr>
				<td><?php echo $no; ?></td>	
				<td><?php echo $rw->nisn ?></td>	
				<td><?php echo $rw->nik ?></td>	
				<td><?php echo strtoupper($rw->nama) ?></td>	
				<td><?php echo strtoupper($rw->tempat_lahir) ?></td>	
				<td><?php echo $rw->tanggal_lahir ?></td>	
				<td><?php echo strtoupper($rw->rt) ?></td>	
				<td><?php echo strtoupper($rw->rw) ?></td>	
				<td><?php echo strtoupper($rw->alamat) ?></td>	
				<td><?php echo strtoupper($rw->no_telp) ?></td>	
				<td><?php echo strtoupper($rw->tahun_daftar) ?></td>	
				<td><?php echo strtoupper($rw->kelas) ?></td>	
				<td><?php echo $rw->biaya_pendaftaran ?></td>	
				<td><?php echo strtoupper($rw->via_bayar) ?></td>	
			</tr>
			<?php $no++; } ?>
		</tbody>
	</table>
	

</body>
</html>