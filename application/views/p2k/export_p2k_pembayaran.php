<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php 
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=data_pembayaran_p2k.xls");
	$bulan = $this->input->get('bulan');
	$tahun = $this->input->get('tahun');
	 ?>

	<center>
		<h2>PEMBAYARAN DARI P2K</h2>
	</center>
	<h4>PERIODE : <?php echo bulan_indo($bulan).' '.$tahun; ?></h4>
	<br><br>

	<table border="1">
		<thead>
			<tr>
				<th rowspan="2">NO.</th>
				<th rowspan="2">NAMA MAHASISWA</th>
				<th rowspan="2">NIM</th>
				<th rowspan="2">SMT</th>
				<th rowspan="2">JURUSAN</th>
				<th style="background-color: orange; color: white;" colspan="2">KEWAJIBAN</th>
				<th style="background-color: lightgreen; color: white;" colspan="2">TERBAYAR</th>
				<th style="background-color: lightblue; color: white;" colspan="2">TUNGGAKAN</th>
				<th rowspan="2">TOTAL TUNGGAKAN</th>
				<th rowspan="2">STATUS</th>
			</tr>
			<tr>
				<th>SPB</th>
				<th>SPP</th>
				<th>SPB</th>
				<th>SPP</th>
				<th>SPB</th>
				<th>SPP</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			$no = 1;
			if ($bulan !='') {
				$this->db->where('bulan', $bulan);
			}
			$this->db->where('tahun', $tahun);
			$data = $this->db->get('p2k_pembayaran');
			foreach ($data->result() as $rw) {
				$nama = get_data('student_mahasiswa','nim',$rw->nim,'nama');
				$konsentrasi_id = get_data('student_mahasiswa','nim',$rw->nim,'konsentrasi_id');
				$prodi = get_data('akademik_konsentrasi','konsentrasi_id',$konsentrasi_id,'nama_konsentrasi');
				$tunggakan1 = $rw->kewajiban_spb - $rw->terbayar_spb;
				$tunggakan2 = $rw->kewajiban_spp - $rw->terbayar_spp;
				$total_tunggakan = $tunggakan1 + $tunggakan2;
			 ?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo strtoupper($nama); ?></td>
				<td><?php echo strtoupper($rw->nim); ?></td>
				<td><?php echo strtoupper($rw->semester); ?></td>
				<td><?php echo strtoupper($prodi); ?></td>
				<td><?php echo $rw->kewajiban_spb ?></td>
				<td><?php echo $rw->kewajiban_spp ?></td>
				<td><?php echo $rw->terbayar_spb ?></td>
				<td><?php echo $rw->terbayar_spp ?></td>
				<td><?php echo $tunggakan1 ?></td>
				<td><?php echo $tunggakan2 ?></td>
				<td><?php echo $total_tunggakan ?></td>
				<td><b><?php echo $rw->status ?></b></td>
			</tr>
			<?php $no++; } ?>
		</tbody>
	</table>
	

</body>
</html>