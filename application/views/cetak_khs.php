<body onload="window.print()">

</body>
<style type="text/css">
    body
    {
        font-family: sans-serif;
        font-size: 14px;
    }
    th{
        padding: 5px;
        font-weight: bold;
        font-size: 12px;
    }
    td{
        font-size: 12px;
        padding: 3px;
    }
    h2{
        text-align: left;
        margin-bottom: 13px;
    }
    .potong
    {
        page-break-after:always;
    }
</style>

<?php $this->load->view('kop'); ?>

<h3 align="center">KARTU HASIL STUDI (KHS)</h3><br>

<table border="1" style="border-collapse: collapse;width: 100%;">
    <tr>
    <td style="width: 100px;">Nama</td><td align="left">: <?php echo strtoupper($profile['nama'])?></td>
    <?php $kode = $profile['nim']; ?>
    <!-- <td width="20" rowspan="4" align="right"><img src="<?php echo base_url('laporan/barcode/'.$kode) ?>" alt=""></td> -->
    </tr>
    <tr><td style="width: 100px;">NPM</td><td>: <?php echo strtoupper($profile['nim']);?></td></tr>
    <tr><td style="width: 100px;">Program Studi</td><td>: <?php echo strtoupper($profile['nama_konsentrasi'])?></td></tr>
</table>
<br>
<center>
    <h4> SEMESTER <?php echo $this->uri->segment(4) ?></h4>
</center>
<table border="1" style="border-collapse: collapse;width: 100%;">

    <tr>
        <th width="10">NO</th>
        <th>KODE</th>
        <th>MATA KULIAH</th>
        <!-- <th>NILAI</th> -->
        <th>HURUF MUTU</th>
        <th>ANGKA MUTU</th>
        <th>SKS</th>
        <th>MUTU</th>
    </tr>
    <?php
    $nim = $this->uri->segment(3);
    $semester = $this->uri->segment(4);
    $no =1 ;
    $sks = 0;
    $mutu = 0;
    foreach ($this->db->get_where('v_khs', array('nim'=>$nim, 'semester'=>$semester, 'confirm'=>1))->result() as $r) {
        ?>
            <tr>
                <td align="center"><?php echo $no++; ?></td>
                <td align="center" width="60"><?php echo strtoupper($r->kode_makul) ?></td>
                <td style="padding-left: 10px;"><?php echo strtoupper($r->nama_makul) ?></td>
                <!-- <td align="center" width="40"><?php echo $r->nilai ?></td> -->
                <td align="center" width="40"><?php echo $r->grade ?></td>
                <td align="center" width="40"><?php echo $r->mutu ?></td>
                <td align="center" width="40"><?php echo $r->sks ?></td>
                <td align="center" width="40"><?php echo $r->mutu*$r->sks ?></td>
                
            </tr>
        <?php
        $sks = $sks+$r->sks;
        $mutu = $mutu + $r->mutu*$r->sks;
    }

    ?>
    
    <tr>
       <td colspan="4">
           <table style="width: 100%;">
    
                <tr>
                    <td style="padding-bottom: 60px;" align="center">Mengetahui, <br> KaProdi <?php echo strtoupper($profile['nama_konsentrasi'])?></td>
                    <td style="padding-bottom: 60px;" align="center">
                        Jakarta, <?php echo tgl_indo(substr(waktu(), 0, 10)) ?> <br>
                        KA. BAAK</td>
                </tr>
                
                <tr>
                       
                    
                    <?php $ka_prodi = get_data('app_dosen','dosen_id',$profile['ka_prodi'],'nama_lengkap'); ?>
                    <?php $nidn = get_data('app_dosen','dosen_id',$profile['ka_prodi'],'nidn'); ?>
                    <td align="center"><u>(<?php echo $ka_prodi; ?>)</td>
                    <td align="center"><u>(Erlinda Sinaga, S.Pd,.MBA)</u> </td>
                    
                </tr>

            </table>

       </td> 
       <td colspan="3">
           <table>
               <tr>
                   <th align="left">Jumlah Mutu</th>
                   <th><?php echo $mutu ?></th>
               </tr>
               <tr>
                   <th align="left">Jumlah SKS yang diperoleh</th>
                   <th><?php echo $sks ?></th>
               </tr>
               <tr>
                   <th align="left">Indeks Prestasi Semester</th>
                   <th><?php echo number_format(ip($nim,$semester),2);?></th>
               </tr>
           </table>
       </td>
    </tr>
</table>

