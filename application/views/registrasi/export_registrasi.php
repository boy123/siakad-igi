<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=export_registrasi_mahasiswa.xls");
?>
<h2>Registrasi Mahasiswa Tahun <?php echo date('Y') ?></h2>

<table border="1">
	<tr>
		<th>No.</th>
		<th>Nim</th>
		<th>Nama</th>
		<th>Semester</th>
		<th>Tgl Registrasi</th>
	</tr>

	<?php
	$no = 1;
	$this->db->like('tanggal_registrasi', date('Y'), 'after');
	 foreach ($this->db->get('akademik_registrasi')->result() as $rw): ?>
	<tr>
		<td><?php echo $no; ?></td>
		<td><?php echo $rw->nim ?></td>
		<td><?php echo get_data('student_mahasiswa','nim',$rw->nim,'nama'); ?></td>
		<td><?php echo $rw->semester ?></td>
		<td><?php echo $rw->tanggal_registrasi ?></td>
	</tr>
	<?php $no++; endforeach ?>
</table>