<div class="row">
	<div class="col-md-6">
		<div style="padding: 10px; margin-bottom: 10px;">
			<h4>Import Tagihan</h4>
			<hr>
			<form action="<?php echo base_url() ?>tagihan_dinamis/import_excel" method="POST" enctype="multipart/form-data">
				<div style="text-align: right">
					<a href="<?php echo base_url() ?>excel/template_pembayaran_dinamis.xlsx" target="_blank" class="label label-success">Download Template XLS</a>
				</div>
				<div class="form-group">
					<label>Upload File</label>
					<input type="file" name="file_excel" class="form-control">
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Import</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
	
		<table id="datatable" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>No.</th>
					<th>Label Tagihan</th>
					<th>Nim</th>
					<th>Nama</th>
					<th>Semester</th>
					<th>Tgl Bayar</th>
					<th>Status</th>
					<th>Option</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$no = 1;
				$this->db->order_by('id_tagihan', 'desc');
				$sql=$this->db->get('tagihan_dinamis');
				foreach ($sql->result() as $rw) {
				 ?>
				<tr>
					<td><?php echo $no ?></td>
					<td><?php echo $rw->label_tagihan ?></td>
					<td><?php echo $rw->nim ?></td>
					<td><?php echo get_data('student_mahasiswa','nim',$rw->nim,'nama') ?></td>
					<td><?php echo $rw->semester ?></td>
					<td><?php echo $rw->tgl_bayar ?></td>
					<td><?php echo ($rw->status == 'paid') ? '<span class="label label-success">PAID</span>' : '<span class="label label-warning">UNPAID</span>' ?></td>
					<td>
						<?php if ($rw->status == 'unpaid'): ?>
							<a href="<?php echo base_url() ?>tagihan_dinamis/delete/<?php echo $rw->id_tagihan ?>" class="label label-danger">Hapus</a>
						<?php endif ?>
					</td>
				</tr>
				<?php $no++;} ?>
			</tbody>
		</table>
	</div>
</div>