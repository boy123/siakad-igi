<body onload="window.print()">

</body>
<style type="text/css">
    body
    {
        font-family: sans-serif;
        font-size: 14px;
    }
    th{
        padding: 5px;
        font-weight: bold;
        font-size: 12px;
    }
    td{
        font-size: 12px;
        padding: 3px;
    }
    h2{
        text-align: left;
        margin-bottom: 13px;
    }
    .potong
    {
        page-break-after:always;
    }
</style>

<?php $this->load->view('kop'); ?>

<h3 align="center">TRANSKIP NILAI</h3><br>

<table border="0" style="border-collapse: collapse;width: 100%;">
    <tr>
    <td style="width: 100px;">Nama</td><td align="left">: <b><?php echo strtoupper($profile['nama'])?></b></td>
    <?php $kode = $profile['nim']; ?>
    <td align="right">Program Pendidikan :</td>
    <td align="right">SARJANA (S1)</td>
    </tr>
    <tr><td style="width: 200px;">Tempat tanggal lahir</td><td>: <?php echo strtoupper($profile['tempat_lahir'].', '.$profile['tanggal_lahir'])?></td>
    <td rowspan="4" align="right">Program Studi :</td>
    <td width="100" rowspan="4" align="right"><?php echo strtoupper($profile['nama_konsentrasi'])?></td>
    </tr>
    <tr><td style="width: 100px;">NPM</td><td>: <?php echo strtoupper($profile['nim']) ?></td>
        
    </tr>
    
</table>
<br>
<table border="1" style="border-collapse: collapse;width: 100%;">

    <tr>
        <th width="10">SMT</th>
        <th>KODE MK</th>
        <th>MATA KULIAH</th>
        <th>HM</th>
        <th>AM</th>
        <th>K</th>
        <th>M</th>
    </tr>
    <?php
    $nim = $this->uri->segment(3);
    // $semester = $this->uri->segment(4);
    $no =1 ;
    $sks = 0;
    // foreach ($this->db->get_where('v_khs', array('nim'=>$nim))->result() as $r) {
    foreach ($this->db->query("SELECT semester FROM v_khs WHERE nim='$nim' GROUP BY semester ")->result() as $h) {

        $this->db->where('confirm', 1);
        $this->db->where('nim', $nim);
        $this->db->where('semester', $h->semester);
        $sql = $this->db->get('v_khs');
        $jml_d = $sql->num_rows()+1;
        // log_r($jml_d);
        ?>
        <tr>
            <td align="center" rowspan="<?php echo $jml_d ?>"><?php echo $h->semester ?></td>
        </tr>
        <?php
        $total_mutu= 0;
        foreach ($sql->result() as $r) {
        ?>
            <tr>
                
                <td align="center" width="60"><?php echo strtoupper($r->kode_makul) ?></td>
                <td style="padding-left: 10px;"><?php echo strtoupper($r->nama_makul) ?></td>
                <td align="center" width="40"><?php echo $r->grade ?></td>
                <td align="center" width="40"><?php echo $r->mutu ?></td>
                <td align="center" width="40"><?php echo $r->sks ?></td>
                <td align="center" width="40"><?php echo $r->mutu * $r->sks; $total_mutu = $total_mutu + ($r->mutu * $r->sks); ?></td>
            </tr>
        <?php
        $sks = $sks+$r->sks;
    }
}

    ?>
    <tr>
        <td align="left" colspan="3"><b>Judul Tugas Akhir</b></td>
        <td align="left" colspan="4"><b><?php echo get_data('student_mahasiswa','nim',$profile['nim'],'judul_skripsi');?></b></td>
    </tr>
    <tr>
        <td align="left" colspan="3"><b>Jumlah Mutu</b></td>
        <td align="left" colspan="4"><b><?php echo $total_mutu;?></b></td>
    </tr>
    <tr>
        <td align="left" colspan="3"><b>Jumlah Kredit Kumulatif</b></td>
        <td align="left" colspan="4"><b><?php echo $sks;?></b></td>
    </tr>
    <tr>
        <td align="left" colspan="3"><b>Index Prestasi Kumulatif (IPK)</b></td>
        <td align="left" colspan="4"><b><?php echo number_format(ipk($nim),2);?></b></td>
    </tr>
</table>

<br><br>
<table style="width: 100%;">
    <tr>
        <td align="right" colspan="3" style="padding-right: 20px; padding-bottom: 15px;">Jakarta, <?php echo tgl_indo(substr(waktu(), 0, 10)) ?></td>
    </tr>
    <tr>
        <td style="padding-bottom: 60px;" align="center">Wakil Ketua I</td>
        <td style="padding-bottom: 60px;" align="center">KA. BAAK</td>
    </tr>
    <tr>
        
        <td align="center"><?php //echo $dosen_pa ?></td>
        <td align="center"><?php //echo strtoupper($profile['nama']) ?></td>
    </tr>

</table>
