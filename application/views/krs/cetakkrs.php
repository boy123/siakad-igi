<body>

</body>
<style type="text/css">
    body
    {
        font-family: sans-serif;
        font-size: 14px;
    }
    th{
        padding: 5px;
        font-weight: bold;
        font-size: 12px;
    }
    td{
        font-size: 12px;
        padding: 3px;
    }
    h2{
        text-align: left;
        margin-bottom: 13px;
    }
    .potong
    {
        page-break-after:always;
    }
</style>

<?php $this->load->view('kop'); ?>

<h3 align="center">KARTU RENCANA STUDI</h3>
<h3 align="center"><?php echo $tahun_akademik ?></h3><br>

<table border="0" style="border-collapse: collapse;width: 100%;">
    <tr>
    
    <td style="width: 100px;">Nama</td><td align="left">: <?php echo strtoupper($profile['nama'])?></td>
    <?php $kode = $profile['nim']; ?>
    <!-- <td width="20" rowspan="4" align="right"><img src="<?php echo base_url('laporan/barcode/'.$kode) ?>" alt=""></td> -->
    </tr>
    <tr><td style="width: 100px;">NPM / Semester</td><td>: <?php echo strtoupper($profile['nim']). " / " . $this->uri->segment(4) ?></td></tr>
    <tr><td style="width: 100px;">Jurusan</td><td>: <?php echo strtoupper($profile['nama_prodi'])?></td></tr>
    <tr><td style="width: 100px;">Prodi</td><td>: <?php echo strtoupper($profile['nama_konsentrasi'])?></td></tr>
    <tr><td style="width: 100px;">Kelas</td><td>: <?php echo strtoupper($profile['kelas_kuliah'])?></td></tr>
</table>
<br>
<table border="1" style="border-collapse: collapse;width: 100%;">

    <tr>
        <th width="10">No</th>
        <th>KODE</th>
        <th>MATA KULIAH</th>
        <th>SKS</th>
        <th>HARI</th>
        <th>KELAS</th>
        <th>WAKTU</th>
        <th>NAMA DOSEN</th>
    </tr>
    
    <?php
    $no =1 ;
    $sks = 0;
    foreach ($record2 as $r) {
        ?>
            <tr>
                <td align="center"><?php echo $no++; ?></td>
                <td align="center" width="60"><?php echo strtoupper($r->kode_makul) ?></td>
                <td style="padding-left: 10px;"><?php echo strtoupper($r->nama_makul) ?></td>
                <td align="center" width="40"><?php echo $r->sks ?></td>
                <td align="center" width="40"><?php echo strtoupper(get_data('app_hari','hari_id',$r->hari_id,'hari')) ?></td>
                <td align="center" width="40"><?php echo strtoupper(get_data('app_ruangan','ruangan_id',$r->ruangan_id,'nama_ruangan')) ?></td>
                <td align="center" width="40"><?php echo $r->jam_mulai.'-'.$r->jam_selesai ?></td>
                <td align="left" width="40"><?php echo strtoupper(get_data('app_dosen','dosen_id',$r->dosen_id,'nama_lengkap')) ?></td>
            </tr>
        <?php
        $sks = $sks+$r->sks;
    }

    ?>
    <tr>
        <td align="center" colspan="3"><b>Total SKS</b></td>
        <td align="center"><b><?php echo  $sks;?></b></td>
        <td colspan="4"></td>
    </tr>
</table>

<br><br>
<table style="width: 100%;">
    <tr>
        <td align="right" colspan="3" style="padding-right: 20px; padding-bottom: 15px;">Jakarta, <?php echo tgl_indo(substr(waktu(), 0, 10)) ?></td>
    </tr>
    <tr>
        <td style="padding-bottom: 60px;" align="center">Mengetahui, <br> KA. PRODI <br>
        <?php echo strtoupper($profile['nama_konsentrasi']) ?> </td>
        <td style="padding-bottom: 60px;" align="center">Menyetujui, <br> Pembimbing Akademik</td>
        <td style="padding-bottom: 60px;" align="center">Mahasiswa</td>
    </tr>
    <tr>
        
        <td align="center"><?php echo $ka_prodi ?></td>
        <td align="center"><?php echo $dosen_pa ?></td>
        <td align="center"><?php echo strtoupper($profile['nama']) ?></td>
    </tr>

</table>
