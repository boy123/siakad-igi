
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Tahun Angkatan <?php echo form_error('angkatan_id') ?></label>
            <!-- <input type="text" class="form-control" name="angkatan_id" id="angkatan_id" placeholder="Angkatan Id" value="<?php echo $angkatan_id; ?>" /> -->
            <select name="angkatan_id" class="form-control">
                <option value="<?php echo $angkatan_id ?>"><?php echo get_data('student_angkatan','angkatan_id',$angkatan_id,'keterangan') ?></option>
                <?php foreach ($this->db->get('student_angkatan')->result() as $rw): ?>
                    <option value="<?php echo $rw->angkatan_id ?>"><?php echo $rw->keterangan ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="int">Jumlah <?php echo form_error('jumlah') ?></label>
            <input type="text" class="form-control" name="jumlah" id="jumlah" placeholder="Jumlah" value="<?php echo $jumlah; ?>" />
        </div>
	    <input type="hidden" name="id_set_spp" value="<?php echo $id_set_spp; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('set_spp') ?>" class="btn btn-default">Cancel</a>
	</form>
   