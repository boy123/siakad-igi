<div class="row">
	<div class="col-md-6">
		<form action="<?php echo base_url() ?>spp_bulanan/cetak_keuangan" method="POST">
			<div class="form-group">
				<label>Bulan</label>
				<select class="form-control" name="bulan" >
					<option value="">--ALL--</option>
					<option value="01">01</option>
					<option value="02">02</option>
					<option value="03">03</option>
					<option value="04">04</option>
					<option value="05">05</option>
					<option value="06">06</option>
					<option value="07">07</option>
					<option value="08">08</option>
					<option value="09">09</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
				</select>
			</div>
			<div class="form-group">
				<label>Tahun</label>
				<select class="form-control" name="tahun" required="">
					<option value="<?php echo date('Y') ?>"><?php echo date('Y') ?></option>
					<option value="<?php echo (date('Y')-1) ?>"><?php echo (date('Y')-1) ?></option>
					<option value="<?php echo (date('Y')-2) ?>"><?php echo (date('Y')-2) ?></option>
				</select>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Cetak</button>
			</div>
		</form>
		
	</div>
</div>