<div class="row">
	<div class="col-md-6">
		<form action="" method="GET">
			<div class="form-group">
				<label>Bulan</label>
				<select class="form-control" name="bulan" >
					<option value="">--ALL--</option>
					<option value="01">01</option>
					<option value="02">02</option>
					<option value="03">03</option>
					<option value="04">04</option>
					<option value="05">05</option>
					<option value="06">06</option>
					<option value="07">07</option>
					<option value="08">08</option>
					<option value="09">09</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
				</select>
			</div>
			<div class="form-group">
				<label>Tahun</label>
				<select class="form-control" name="tahun" required="">
					<option value="<?php echo date('Y') ?>"><?php echo date('Y') ?></option>
					<option value="<?php echo (date('Y')-1) ?>"><?php echo (date('Y')-1) ?></option>
					<option value="<?php echo (date('Y')-2) ?>"><?php echo (date('Y')-2) ?></option>
				</select>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Cari</button>
			</div>
		</form>
		<a href="<?php echo base_url() ?>spp_bulanan/buat_tagihan" class="btn btn-info" onclick="return confirm('apakah kamu yakin akan buat tagihan <?php echo date('m').'-'.date('Y') ?>?');">+ Buat Tagihan</a>
	</div>

	<div class="col-md-6">
		<br>
		
	</div>
</div>

<div class="row">
	<div class="col-md-12">
	<?php 
	if (isset($_GET)) {
	 ?>
		<table id="datatable" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>No.</th>
					<th>Nim</th>
					<th>Nama</th>
					<th>Bulan</th>
					<th>Tahun</th>
					<th>Tgl Bayar</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$bulan = $this->input->get('bulan');
				$tahun = $this->input->get('tahun');
				$no = 1;
				if ($bulan !='') {
					$this->db->where('bulan', $bulan);
				}
				$this->db->where('tahun', $tahun);
				$sql=$this->db->get('spp_bulanan');
				foreach ($sql->result() as $rw) {
				 ?>
				<tr>
					<td><?php echo $no ?></td>
					<td><?php echo $rw->nim ?></td>
					<td><?php echo get_data('student_mahasiswa','nim',$rw->nim,'nama') ?></td>
					<td><?php echo $rw->bulan ?></td>
					<td><?php echo $rw->tahun ?></td>
					<td><?php echo $rw->tgl_bayar ?></td>
					<td><?php echo ($rw->status == 'paid') ? '<span class="label label-success">PAID</span>' : '<span class="label label-danger">UNPAID</span>' ?></td>
				</tr>
				<?php $no++;} ?>
			</tbody>
		</table>
	<?php } ?>
	</div>
</div>