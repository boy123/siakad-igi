<div class="row">
	<div class="col-md-12">
		<form action="simpan_bayar_spp" method="POST">
			<div class="form-group">
				
				<div class="row">
					<div class="col-md-4">
						<label>Nim</label>
						<input type="text" name="nim" class="form-control">
					</div>
					<div class="col-md-4">
						<label>Periode</label>
						<select name="bulan" class="form-control">
							<option value="">--Pilih Bulan--</option>
							<?php 
							for ($i=1; $i <= 12 ; $i++) { 
							 ?>
							<option value="<?php echo $i ?>"><?php echo bulan_indo($i) ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-md-4">
						<label>Tahun</label>
						<input type="text" name="tahun" class="form-control" value="<?php echo date('Y') ?>" readonly>
					</div>
					
				</div>
				
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-info">Bayar Sekarang</button>
			</div>
		</form>
	</div>
</div>