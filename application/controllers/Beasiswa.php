<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Beasiswa extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Beasiswa_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'beasiswa/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'beasiswa/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'beasiswa/index.html';
            $config['first_url'] = base_url() . 'beasiswa/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Beasiswa_model->total_rows($q);
        $beasiswa = $this->Beasiswa_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'beasiswa_data' => $beasiswa,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'title' => 'Set Beasiswa',
        );
        $this->template->load('template','beasiswa/beasiswa_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Beasiswa_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_beasiswa' => $row->id_beasiswa,
		'nim' => $row->nim,
		'jumlah' => $row->jumlah,
	    );
            $this->template->load('beasiswa/beasiswa_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('beasiswa'));
        }
    }

    public function create() 
    {
        $data = array(
            'title' => 'Set Beasiswa',
            'button' => 'Create',
            'action' => site_url('beasiswa/create_action'),
	    'id_beasiswa' => set_value('id_beasiswa'),
	    'nim' => set_value('nim'),
	    'jumlah' => set_value('jumlah'),
	);
        $this->template->load('template','beasiswa/beasiswa_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nim' => $this->input->post('nim',TRUE),
		'jumlah' => $this->input->post('jumlah',TRUE),
	    );

            $this->Beasiswa_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('beasiswa'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Beasiswa_model->get_by_id($id);

        if ($row) {
            $data = array(
                'title' => 'Set Beasiswa',
                'button' => 'Update',
                'action' => site_url('beasiswa/update_action'),
		'id_beasiswa' => set_value('id_beasiswa', $row->id_beasiswa),
		'nim' => set_value('nim', $row->nim),
		'jumlah' => set_value('jumlah', $row->jumlah),
	    );
            $this->template->load('template','beasiswa/beasiswa_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('beasiswa'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_beasiswa', TRUE));
        } else {
            $data = array(
		'nim' => $this->input->post('nim',TRUE),
		'jumlah' => $this->input->post('jumlah',TRUE),
	    );

            $this->Beasiswa_model->update($this->input->post('id_beasiswa', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('beasiswa'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Beasiswa_model->get_by_id($id);

        if ($row) {
            $this->Beasiswa_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('beasiswa'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('beasiswa'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nim', 'nim', 'trim|required');
	$this->form_validation->set_rules('jumlah', 'jumlah', 'trim|required');

	$this->form_validation->set_rules('id_beasiswa', 'id_beasiswa', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Beasiswa.php */
/* Location: ./application/controllers/Beasiswa.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2020-07-21 17:00:31 */
/* https://jualkoding.com */