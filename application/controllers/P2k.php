<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class P2k extends CI_Controller {

	
	public function add_pmb()
	{

		// $header_value = request_header("Authorization");
		$header_value = $this->input->request_headers("Authorization");

		$base64_value = str_replace("Bearer ", "", $header_value);

		$base64_value = base64_decode($base64_value['Authorization']);

		$value_array = explode(":", $base64_value);

		if ($value_array[0] === "siakadigi" && $value_array[1] === "2020") {
			
			if ($_POST) {
				if ($this->input->post('nisn') == '') {
					$pesan = array(
						'status' => 0,
						'pesan' => 'nisn tidak boleh kosong'
					);
					echo json_encode($pesan);
					exit();
				}
				$data = array(
					"nisn" => $this->input->post('nisn'),
					"nik" => $this->input->post('nik'),
					"nama" => $this->input->post('nama'),
					"tempat_lahir" => $this->input->post('tempat_lahir'),
					"tanggal_lahir" => $this->input->post('tanggal_lahir'),
					"alamat" => $this->input->post('alamat'),
					"rt" => $this->input->post('rt'),
					"rw" => $this->input->post('rw'),
					"no_hp" => $this->input->post('no_hp'),
					"biaya_pendaftaran" => $this->input->post('biaya_pendaftaran'),
					"tahun_daftar" => $this->input->post('tahun_daftar'),
					"kelas" => $this->input->post('kelas'),
					"via_bayar" => $this->input->post('via_bayar'),
					"date_create" => get_waktu()
				);

				// $this->db->where('nisn', $this->input->post('nisn'));
				// $this->db->where('nik', $this->input->post('nik'));
				// $cek_ = $this->db->get('p2k_pmb');
				// if ($cek_->num_rows() > 0) {
				// 	$this->db->where('Field / comparison', $Value);
				// }

				$simpan = $this->db->insert('p2k_pmb', $data);
				if ($simpan) {
					$pesan = array(
						'status' => 1,
						'pesan' => 'data berhasil disimpan'
					);
					echo json_encode($pesan);
				} else {
					$pesan = array(
						'status' => 0,
						'pesan' => 'data gagal disimpan'
					);
					echo json_encode($pesan);
				}
				
				

			} else {
				$pesan = array(
					'status' => 0,
					'pesan' => 'tidak ada data yang di POST'
				);
				echo json_encode($pesan);
			}

		} else {
			
			$pesan = array(
				'status' => 0,
				'pesan' => 'credensial salah'
			);
			echo json_encode($pesan);
		}
	}

	public function add_pembayaran()
	{

		// $header_value = request_header("Authorization");
		$header_value = $this->input->request_headers("Authorization");

		$base64_value = str_replace("Bearer ", "", $header_value);

		$base64_value = base64_decode($base64_value['Authorization']);

		$value_array = explode(":", $base64_value);

		if ($value_array[0] === "siakadigi" && $value_array[1] === "2020") {
			
			if ($_POST) {
				if ($this->input->post('nim') == '') {
					$pesan = array(
						'status' => 0,
						'pesan' => 'nim tidak boleh kosong'
					);
					echo json_encode($pesan);
					exit();
				}
				$data = array(
					"nim" => $this->input->post('nim'),
					"semester" => $this->input->post('semester'),
					"kewajiban_spb" => $this->input->post('kewajiban_spb'),
					"kewajiban_spp" => $this->input->post('kewajiban_spp'),
					"terbayar_spb" => $this->input->post('terbayar_spb'),
					"terbayar_spp" => $this->input->post('terbayar_spb'),
					"bulan" => $this->input->post('bulan'),
					"tahun" => $this->input->post('tahun'),
					"status" => $this->input->post('status'),
					"date_create" => get_waktu()
				);

				$this->db->where('nim', $this->input->post('nim'));
				$this->db->where('semester', $this->input->post('semester'));
				$cek_ = $this->db->get('p2k_pembayaran');
				if ($cek_->num_rows() > 0) {
					//update data
					$this->db->where('nim', $this->input->post('nim'));
					$this->db->where('semester', $this->input->post('semester'));
					$update = $this->db->update('p2k_pembayaran', array(
						"kewajiban_spb" => $this->input->post('kewajiban_spb'),
						"kewajiban_spp" => $this->input->post('kewajiban_spp'),
						"terbayar_spb" => $this->input->post('terbayar_spb'),
						"terbayar_spp" => $this->input->post('terbayar_spb'),
						"bulan" => $this->input->post('bulan'),
						"tahun" => $this->input->post('tahun'),
						"status" => $this->input->post('status'),
						"date_update" => get_waktu()
					));
					if ($update) {
						$pesan = array(
							'status' => 1,
							'pesan' => 'data berhasil diupdate'
						);
						echo json_encode($pesan);
					} else {
						$pesan = array(
							'status' => 0,
							'pesan' => 'data gagal diupdate'
						);
						echo json_encode($pesan);
					}
				} else {
					$simpan = $this->db->insert('p2k_pembayaran', $data);
					if ($simpan) {
						$pesan = array(
							'status' => 1,
							'pesan' => 'data berhasil disimpan'
						);
						echo json_encode($pesan);
					} else {
						$pesan = array(
							'status' => 0,
							'pesan' => 'data gagal disimpan'
						);
						echo json_encode($pesan);
					}
				}

				
				
				

			} else {
				$pesan = array(
					'status' => 0,
					'pesan' => 'tidak ada data yang di POST'
				);
				echo json_encode($pesan);
			}

		} else {
			
			$pesan = array(
				'status' => 0,
				'pesan' => 'credensial salah'
			);
			echo json_encode($pesan);
		}
	}


	public function index()
	{
		$data['title']  = 'Export Data P2K';
    	$this->template->load('template', 'p2k/view',$data);
	}

	public function export_p2k_pmb()
	{
		$this->load->view('p2k/export_p2k_pmb');
	}

	public function export_p2k_pembayaran()
	{
		$this->load->view('p2k/export_p2k_pembayaran');
	}


}
