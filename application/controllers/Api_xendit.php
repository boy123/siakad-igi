<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Xendit\Xendit;

class Api_xendit extends CI_Controller {

	
	public function in_pay()
	{
		if ($_SERVER["REQUEST_METHOD"] === "POST") {
	        $data = file_get_contents("php://input");
	        log_data("\n\$data contains the updated invoice data \n\n");
	        log_data($data);
	        $d = json_decode($data);
	        $x = explode("_", $d->external_id);
	        if ($x[0] == 'spp') {
	        	$this->db->where('nim', $x[1]);
	        	$this->db->where('bulan', $x[2]);
	        	$this->db->where('tahun', $x[3]);
	        	$this->db->update('spp_bulanan', array(
	        		'tgl_bayar'=>get_waktu(),
	        		'status' => 'paid'
	        	));
	        } elseif ($x[0] == 'nonspp') {
	        	$this->db->where('nim', $x[1]);
	        	$this->db->where('id_tagihan', $x[2]);
	        	$this->db->update('tagihan_dinamis', array(
	        		'tgl_bayar'=>get_waktu(),
	        		'status' => 'paid'
	        	));
	        } elseif ($x[0] == 'maru') {
	        	$this->db->where('no_pendaftaran', $x[1]);
	        	$this->db->update('pendaftaran', array('status_bayar' => 'paid'));
	        }
	        // log_r($d);
	        $this->db->insert('xendit_pembayaran', $d);
	        log_data("\n\nUpdate your database with the invoice status \n\n");
	    } else {
	        log_r("Cannot ".$_SERVER["REQUEST_METHOD"]." ".$_SERVER["SCRIPT_NAME"]);
	    }
	}

	public function tagihan_xendit()
	{
		// input xendit
            require APPPATH.'vendor/autoload.php';
        
            Xendit::setApiKey("xnd_production_GbB7UbArddFYkImHpL1GfuLZQsgk2PNLvBAk8t3rSMIdVrjlFlmCyXclQrczCi7f");

            // $expried_date = expiry_date(get_waktu(),date('2020-05-20 23:59:59'));

            $no_tagihan = $this->input->get('no_tagihan');
            $label = $this->input->get('label');
            $total_tagihan = $this->input->get('total_tagihan');
            $deskripsi = $this->input->get('deskripsi');

            //cek
            $this->db->where('no_tagihan', $no_tagihan);
            $cek = $this->db->get('xendit_tagihan');
            if ($cek->num_rows() > 0) {

            	$getInvoice = \Xendit\Invoice::retrieve($cek->row()->id_xendit);
            	if ($getInvoice['status'] == 'EXPIRED') {
            		// buat tagihan baru

            		$params = ['external_id' => $no_tagihan,
		                'payer_email' => 'tagihan@stie-igi.ac.id',
		                'description' => $deskripsi,
		                'amount' => $total_tagihan,
		                // 'invoice_duration' => $expried_date
		            ];
		            $createInvoice = \Xendit\Invoice::create($params);
		            $id_xendit = $createInvoice['id'];

		            $getInvoice = \Xendit\Invoice::retrieve($id_xendit);
		            // log_data($getInvoice);
		            $url_back = $getInvoice['invoice_url'];

		            $this->db->where('no_tagihan', $no_tagihan);
		            $this->db->update('xendit_tagihan', array(
		            	'id_xendit' => $id_xendit,
		            	'invoice_url' => $url_back,
		            ));

		            header("location:$url_back");

            	} else {
            		$url = $cek->row()->invoice_url;
            		header("location:$url");
            	}

            	
            } else {
            	$params = ['external_id' => $no_tagihan,
	                'payer_email' => 'tagihan@stie-igi.ac.id',
	                'description' => $deskripsi,
	                'amount' => $total_tagihan,
	                // 'invoice_duration' => $expried_date
	            ];
	            $createInvoice = \Xendit\Invoice::create($params);
	            $id_xendit = $createInvoice['id'];

	            $getInvoice = \Xendit\Invoice::retrieve($id_xendit);
	            // log_data($getInvoice);
	            $url_back = $getInvoice['invoice_url'];



	            $this->db->insert('xendit_tagihan', array(
	                'no_tagihan' => $no_tagihan,
	                'amount' => $total_tagihan,
	                'label_tagihan' => $label,
	                'id_xendit'=> $id_xendit,
	                'invoice_url' => $url_back,
	            ));

	            header("location:$url_back");

            }


            
	}

}
