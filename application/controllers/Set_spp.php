<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Set_spp extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Set_spp_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'set_spp/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'set_spp/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'set_spp/index.html';
            $config['first_url'] = base_url() . 'set_spp/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Set_spp_model->total_rows($q);
        $set_spp = $this->Set_spp_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'set_spp_data' => $set_spp,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'title' => 'Set SPP',
        );
        $this->template->load('template','set_spp/set_spp_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Set_spp_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_set_spp' => $row->id_set_spp,
		'angkatan_id' => $row->angkatan_id,
		'jumlah' => $row->jumlah,
	    );
            $this->template->load('set_spp/set_spp_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('set_spp'));
        }
    }

    public function create() 
    {
        $data = array(
            'title' => 'Set SPP',
            'button' => 'Create',
            'action' => site_url('set_spp/create_action'),
	    'id_set_spp' => set_value('id_set_spp'),
	    'angkatan_id' => set_value('angkatan_id'),
	    'jumlah' => set_value('jumlah'),
	);
        $this->template->load('template','set_spp/set_spp_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'angkatan_id' => $this->input->post('angkatan_id',TRUE),
		'jumlah' => $this->input->post('jumlah',TRUE),
	    );

            $this->Set_spp_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('set_spp'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Set_spp_model->get_by_id($id);

        if ($row) {
            $data = array(
                'title' => 'Set SPP',
                'button' => 'Update',
                'action' => site_url('set_spp/update_action'),
		'id_set_spp' => set_value('id_set_spp', $row->id_set_spp),
		'angkatan_id' => set_value('angkatan_id', $row->angkatan_id),
		'jumlah' => set_value('jumlah', $row->jumlah),
	    );
            $this->template->load('template','set_spp/set_spp_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('set_spp'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_set_spp', TRUE));
        } else {
            $data = array(
		'angkatan_id' => $this->input->post('angkatan_id',TRUE),
		'jumlah' => $this->input->post('jumlah',TRUE),
	    );

            $this->Set_spp_model->update($this->input->post('id_set_spp', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('set_spp'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Set_spp_model->get_by_id($id);

        if ($row) {
            $this->Set_spp_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('set_spp'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('set_spp'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('angkatan_id', 'angkatan id', 'trim|required');
	$this->form_validation->set_rules('jumlah', 'jumlah', 'trim|required');

	$this->form_validation->set_rules('id_set_spp', 'id_set_spp', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Set_spp.php */
/* Location: ./application/controllers/Set_spp.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2020-07-21 17:13:15 */
/* https://jualkoding.com */