<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kalender_akademik extends CI_Controller {

	
	public function index()
    {
        $data['title']  = 'Kalender akademik';
        $data['img_kalender'] = get_data('akademik_kalender','id_kalender','1','kalender');
    	$this->template->load('template','kalender/view',$data);
    }

    public function kalender_mahasiswa()
    {
        $data['title']  = 'Kalender akademik';
        $data['img_kalender'] = get_data('akademik_kalender','id_kalender','1','kalender');
        $this->template->load('template','kalender/view_mahasiswa',$data);
    }

    public function update()
    {
    	if ($_FILES) {
    		$return = array();

            $config['upload_path'] = './images/kalender/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $config['max_size'] = '2048';
            $config['overwrite'] = true;
            $config['file_name'] = 'kalender_'.time();

            $this->upload->initialize($config); // Load konfigurasi uploadnya
            if($this->upload->do_upload('userfile')){ // Lakukan upload dan Cek jika proses upload berhasil
                // Jika berhasil :
                $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
                // return $return;
            }else{
                // Jika gagal :
                $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());

                $this->session->set_flashdata('message', alert_biasa($return['error'],'error'));
            	redirect('Kalender_akademik','refresh');
                // return $return;
            }

            $this->db->where('id_kalender', '1');
            $this->db->update('akademik_kalender', array(
            	'kalender' => $return['file']['file_name']
            ));

            $this->session->set_flashdata('message', alert_biasa('Kalender berhasil di ubah !','success'));
            	redirect('Kalender_akademik','refresh');

    	}
    }


}
