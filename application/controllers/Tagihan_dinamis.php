<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tagihan_dinamis extends CI_Controller {

	
	public function index()
	{
		$data['title']  = 'Tagihan Dinamis';
    	$this->template->load('template', 'tagihan_dinamis/view',$data);
	}

	public function import_excel()
    {
        include APPPATH.'third_party/PHPExcel/PHPExcel.php';

        // Fungsi untuk melakukan proses upload file
        $return = array();
        $this->load->library('upload'); // Load librari upload

        $config['upload_path'] = './excel/';
        $config['allowed_types'] = 'xlsx';
        $config['max_size'] = '2048';
        $config['overwrite'] = true;
        $config['file_name'] = 'import_tagihan_dinamis';

        $this->upload->initialize($config); // Load konfigurasi uploadnya
        if($this->upload->do_upload('file_excel')){ // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');

            
            // return $return;
        }else{
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            // return $return;

            $this->session->set_flashdata('message',alert_biasa($return['error'],'error'));
            redirect('tagihan_dinamis','refresh');
        }
        // print_r($return);exit();


        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('excel/import_tagihan_dinamis.xlsx'); // Load file yang tadi diupload ke folder excel
        $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
        
        //skip untuk header
        unset($sheet[1]);

        // log_r($sheet);

        $a = array();

        foreach ($sheet as $rw) {
            
            
            $nim = $rw['D'];
            $jumlah_tagihan = $rw['C'];
            $label_tagihan = $rw['B'];
            $semester = $rw['F'];
            

            if (!empty($rw['B']) OR !empty($rw['C'])) {

                $cek = $this->db->get_where('tagihan_dinamis', array('nim'=>$nim, 'label_tagihan'=>$label_tagihan,'semester'=>$semester));
                if ($cek->num_rows() > 0) {
                    $dt = $cek->row();
                    $this->session->set_flashdata('message',alert_biasa("terdapat data double dengan nim: $dt->nim, label tagihan: $dt->label_tagihan dan semester $semester ",'error'));
                    redirect('tagihan_dinamis','refresh');
                } else {

                

                    $data = array(
                        'nim' => $nim,
                        'semester' => $semester,
                        'label_tagihan' => $label_tagihan,
                        'jumlah_tagihan' => $jumlah_tagihan
                    );

                    array_push($a, $data);

                   
                }
            }
            

        }

        
        
        // log_data($data);
        // log_r($a);
        $simpan_tagihan =$this->db->insert_batch('tagihan_dinamis', $a);

        $this->session->set_flashdata('message',alert_biasa('import data berhasil','success'));
        redirect('tagihan_dinamis','refresh');
    }

    public function delete($id_tagihan)
    {
        $this->db->where('id_tagihan', $id_tagihan);
        $this->db->delete('tagihan_dinamis');
        $this->session->set_flashdata('message',alert_biasa('data berhasil dihapus !','success'));
        redirect('tagihan_dinamis','refresh');
    }




}
