<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spp_bulanan extends CI_Controller {

	public function cetak_keuangan()
	{
		$this->load->view('spp_bulanan/cetak.php', $_POST);
	}

	public function cetak()
	{
		$data['title']  = 'Cetak Keuangan';
    	$this->template->load('template', 'spp_bulanan/v_cetak',$data);
	}
	public function index()
	{
		$data['title']  = 'Tagihan SPP';
    	$this->template->load('template', 'spp_bulanan/spp_bulanan_list',$data);
	}

	public function buat_tagihan()
	{
		$this->db->where('status_mhs', 'Aktif');
		$this->db->where('metode_pembayaran', 'kampus');
		$sql = $this->db->get('student_mahasiswa');
		foreach ($sql->result() as $rw) {
			$data = array(
				'nim'=>$rw->nim,
				'bulan'=>date('m'),
				'tahun'=>date('Y'),
			);
			
			if ($this->db->get_where('spp_bulanan', $data)->num_rows() > 0) {
				log_data('spp sudah di buat sebelumnya di bulan ini');
			} else {
				$this->db->insert('spp_bulanan', $data);
			}
			
		}
		// log_r('berhasil di jalankan');
		$this->session->set_flashdata('message', alert_biasa('Tagihan berhasil di buat ','success'));
            redirect('spp_bulanan','refresh');
	}

	public function bayar_tagihan()
	{
		$data['title']  = 'Bayar SPP';
    	$this->template->load('template', 'spp_bulanan/form_bayar',$data);
	}

	public function simpan_bayar_spp()
	{
		$bulan = $this->input->post('bulan');
		$cek_str = strlen($bulan);
		if ($cek_str == 1) {
			$bulan = '0'.$bulan;
		}
		
		$data = array(
			'nim'=>$this->input->post('nim'),
			'bulan'=>$bulan,
			'tahun'=>$this->input->post('tahun'),
		);

        $cari = $this->db->get_where('spp_bulanan', $data);
        if ($cari->num_rows() == 0) {
            $this->session->set_flashdata('message', alert_biasa('Nim tidak ditemukan','danger'));
            redirect('spp_bulanan/bayar_tagihan','refresh');
        } else {
            $this->db->where('nim', $this->input->post('nim'));
            $this->db->where('bulan', $bulan);
            $this->db->where('tahun', $this->input->post('tahun'));
            $this->db->update('spp_bulanan', array('status'=>'paid','tgl_bayar'=>get_waktu()));

            $this->session->set_flashdata('message', alert_biasa('Pembayaran berhasil di update','success'));
            redirect('spp_bulanan/bayar_tagihan','refresh');
        }
	}


}
